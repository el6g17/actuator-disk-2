// -*- C++ -*-

#ifndef AMROC_PROBLEM_H
#define AMROC_PROBLEM_H

#define DIM 3

#include "LBMProblem.h"
#include "LBMD3Q19.h"

typedef double DataType;
typedef LBMD3Q19<DataType> LBMType;

#define OWN_LBMSCHEME
#include "LBMStdProblem.h"

class LBMSpecific : public LBMType {
  typedef LBMType base;
  typedef base::point_type point_type;
public:

  // Initialize local variables
  LBMSpecific() : base() {
    x0 = 0.;
    Radius=0.;
    CT=0.; lambda=1.; omega=0.;
    pi=4.*std::atan(1.);
  }

  // Register local variables for input from solver.in. See solver.in for the keywords
  virtual void register_at(ControlDevice& Ctrl, const std::string& prefix) {
    base::register_at(Ctrl,prefix);
    RegisterAt(base::LocCtrl,"Center(1)",x0(0)); 
    RegisterAt(base::LocCtrl,"Center(2)",x0(1)); 
    RegisterAt(base::LocCtrl,"Center(3)",x0(2)); 
    RegisterAt(base::LocCtrl,"Radius",Radius); 
    RegisterAt(base::LocCtrl,"CT",CT); 
    RegisterAt(base::LocCtrl,"Lambda",lambda); 
    RegisterAt(base::LocCtrl,"Omega",omega);
  }

  virtual void SetupData(GridHierarchy* gh, const int& ghosts) {
    base::SetupData(gh,ghosts);
    DataType a = 0.5*(1.-std::sqrt(1.-std::abs(CT)));
    if (CT!=0.) Ct = 4.*a/(1.-a)*CT/std::abs(CT);
    else Ct=0.;
    std::cout << "Local axial induction factor: "<< a << std::endl;
    std::cout << "Local thrust coefficient: "<< Ct << std::endl;
  }

  // The routine is called for all cells in every time step for Method(5)>0 to impose the returned force vector.
  // f: Distributions in current cell
  // q: Macroscopic variables in current cell
  // xc: Coordinates of cell center
  // dx: Cell width
  // dt: Time step size
  // fvec: Complete grid
  // i,j,k: indices on complete grid
  // t: Physical time
  // scaling: Physical units and Lattice units possible
  // Physical time is t.
  // For Method(5)=1, q() and force are in SI units; for Method(5)=2 they are in lattice units.
  inline virtual point_type BodyForce(const MicroType &f, MacroType &q, const DCoords &xc, const DCoords &dx, const DataType& dt,
				      const vec_grid_data_type &fvec, const int &i,  const int &j,  const int &k, const DataType& t,
				      const int scaling=0) const {
    point_type force(0.);   
    DCoords dc = xc-x0;
    double rd = std::sqrt(dc(1)*dc(1)+dc(2)*dc(2));
    double rd2 = std::sqrt(dc(1)*dc(1) + (dc(2)+20.)*(dc(2)+20.));

    float gamma=0.;
    gamma = 0.1875*pi; //33.75 deg yaw

    // Impose force on single line of cells only 
    if (dc(0)<0. && -dc(0)<dx(0) && rd<=Radius) {

/*
    // Impose force in yaw for angle gamma
    if (rd<=Radius && rdyaw<=Radius && 
	dc(0)>=(rdyaw*sin(gamma)-dx(0)) && dc(0)<=(rdyaw*sin(gamma)+dx(0)) && 
	dc(2)>=(rdyaw*cos(gamma)-dx(2)) && dc(2)<=(rdyaw*cos(gamma)+dx(2))
	|| rd<=Radius && rdyaw<=Radius &&
	dc(0)>=(-1.*rdyaw*sin(gamma)-dx(0)) && dc(0)<=(-1.*rdyaw*sin(gamma)+dx(0)) && 
	dc(2)>=(-1.*rdyaw*cos(gamma)-dx(2)) && dc(2)<=(-1.*rdyaw*cos(gamma)+dx(2))) {
*/

      // Force computation
      DataType Density = q(0);
      DataType VelU = q(1);
      DataType VelV = q(2);
      DataType VelW = q(3);
      
      force(0) = Ct*DataType(0.5)*Density*VelU*VelU;

      DataType Beta = std::atan2( dc(1), dc(2) );
      DataType mu = rd/Radius;
      // Local angular velocity according to angular momentum theory
      DataType local_omega = 0.5*CT*omega/(lambda*lambda*mu*mu);
      DataType Ut =  local_omega*rd;
      DataType Utry =  Ut*std::cos(Beta); // -VelV;
      DataType Utrz = -Ut*std::sin(Beta); // -VelW;   
      DataType TanForce = DataType(0.5)*Density*(Utry*Utry+Utrz*Utrz);
      force(1) =  TanForce*std::cos(Beta);
      force(2) = -TanForce*std::sin(Beta);
    }
    return force;
  }
  
private:
  DCoords x0;
  DataType Radius, CT, Ct, lambda, omega, pi;
};
 
#endif



