""" Converts dat files to be plotted """

import numpy as np
import matplotlib.pyplot as plt


def convert(filename):
    f = open(filename, 'r')
    data = f.read()
    f.close

    data = data.split()
    del data[0:13]

    "Tabulate r values"
    r = []
    tabulate(data, r, 0)

    "Tabulate density values"
    density = []
    tabulate(data, density, 1)

    "Tabulate velocity_u values"
    velocity_u = []
    tabulate(data, velocity_u, 2)

    "Tabulate velocity_v values"
    velocity_v = []
    tabulate(data, velocity_v, 3)

    "Tabulate velocity_w values"
    velocity_w = []
    tabulate(data, velocity_w, 4)

    "Tabulate velocity_abs values"
    velocity_abs = []
    tabulate(data, velocity_abs, 5)

    "Tabulate pressure values"
    pressure = []
    tabulate(data, pressure, 6)

    "Tabulate distribution values"
    dist = []
    tabulate(data, dist, 7)

    "Tabulate level values"
    level = []
    tabulate(data, level, 8)

    data_array = np.array([r, density, velocity_u, velocity_v, velocity_w,
                           velocity_abs, pressure, dist, level])

    return data_array


def tabulate(data, listname, n):
    for i in xrange(len(data)/9):
        listname.append(data[i*9+n])
    return listname


def plot(dataname, filename1, filename2):

    data_array1 = convert(filename1)
    data_array2 = convert(filename2)

    n = 0

    if dataname == 'r':
        n = 0
    elif dataname == 'density':
        n = 1
    elif dataname == 'velocity_u':
        n = 2
    elif dataname == 'velocity_v':
        n = 3
    elif dataname == 'velocity_w':
        n = 4
    elif dataname == 'velocity_abs':
        n = 5
    elif dataname == 'pressure':
        n = 6
    elif dataname == 'dist':
        n = 7
    elif dataname == 'level':
        n = 8
    else:
        print 'Error: data does not exist'

    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    ax1.set_xlabel('r')
    ax1.set_ylabel(dataname)

    ax1.plot(data_array1[0], data_array1[n], c='r')
    """leg = ax1.legend"""
    plt.show()

    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    ax1.set_xlabel('r')
    ax1.set_ylabel(dataname)

    ax1.plot(data_array2[0], data_array2[n], c='r')
    """leg = ax1.legend"""
    plt.show()


def export(filename):

    data_array = convert(filename)

    f = open("export_" + filename, "w")
    
    for y in xrange(len(data_array[0])):
        for x in range(9):
            f.write(data_array[x,y]+",")
        f.write("\n")

    f.close
    